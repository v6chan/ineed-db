
# Variables

DATA_PATH		= test_data
DB_PATH			= $(DATA_PATH)/db
SEED_PATH		= $(DATA_PATH)/seeds
SEED_SCRIPTS	= $(shell find $(SEED_PATH) -name '*.js')
TERMINAL		= gnome-terminal -x sh -c

# Targets
.PHONY: start
start: db server

.PHONY: server
server:
	$(TERMINAL) "node ./bin/www"

.PHONY: db
db:
	rm -rf $(DB_PATH)
	mkdir $(DB_PATH)
	$(TERMINAL) "mongod --dbpath $(DB_PATH)"
	$(foreach seed, $(SEED_SCRIPTS), node $(seed);)

