import os
import sys
import shutil
import json
import requests as req

tmp_dir      = './tmp'
url          = 'https://ineed-db.mybluemix.net/api/'
tables       = ['members', 'vendors', 'items', 'orders', 'transactions']
state_tables = [
    ('orders', 'order_state', 'orderId'),
    ('transactions', 'transaction_state', 'transactionId')
]

def import_data():
    print('Retrieving data and placing it in %s' % tmp_dir)

    if os.path.isdir(tmp_dir):
        shutil.rmtree(tmp_dir)

    os.mkdir(tmp_dir)

    for table in tables:
        url_endpoint = url + table
        filename = tmp_dir + '/' + table + '.json'
        with open(filename, 'w') as fid:
            res = req.get(url_endpoint)
            if res.status_code == 200:
                print('GET Table %s -- Status: %d; Entry Count: %d' % (table, res.status_code, len(res.json())))
                fid.write(res.text)
            else:
                print('GET Table %s -- Status: %d; ERROR' % (table, res.status_code))
                continue

    for table, stable, sid in state_tables:
        url_endpoint = url + table + '/*/' + stable
        filename = tmp_dir + '/' + stable + '.json'
        with open(filename, 'w') as fid:
            res = req.get(url_endpoint)
            if res.status_code == 200:
                print('GET Table %s -- Status: %d; Entry Count: %d' % (stable, res.status_code, len(res.json())))
                fid.write(res.text)
            else:
                print('GET Table %s -- Status: %d; ERROR' % (stable, res.status_code))
                continue

    print('Done!')

def export_data():
    print('Pushing data found in %s' % tmp_dir)

    for table in tables:
        url_endpoint = url + table
        filename = tmp_dir + '/' + table + '.json'
        with open(filename, 'r') as fid:
            success = 0
            data = json.load(fid)
            for obj in data:
                res = req.post(url_endpoint, obj)
                if res.status_code == 200:
                    success = success + 1
            print('Post Table %s -- Success: %d of %d' % (table, success, len(data)))

    for table, stable, sid in state_tables:
        filename = tmp_dir + '/' + stable + '.json'
        with open(filename, 'r') as fid:
            success = 0
            data = json.load(fid)
            for obj in data:
                url_endpoint = url + table + '/' + obj[sid] + '/' + stable
                res = req.post(url_endpoint, obj)
                if res.status_code == 200:
                    success = success + 1
            print('Post Table %s -- Success: %d of %d' % (stable, success, len(data)))

    print('Done!')

def delete_data():

    print('Deleting data based on entries in %s' % tmp_dir)

    for table in tables:
        filename = tmp_dir + '/' + table + '.json'
        with open(filename, 'r') as fid:
            success = 0
            data = json.load(fid)
            for obj in data:
                obj_id = obj['_id']
                if table == 'members':
                    obj_id = obj['email']
                url_endpoint = url + table + '/' + str(obj_id)
                res = req.delete(url_endpoint)
                if res.status_code == 200:
                    success = success + 1
            print('DELETE Table %s -- Success: %d of %d' % (table, success, len(data)))

    for table, stable, sid in state_tables:
        filename = tmp_dir + '/' + stable + '.json'
        with open(filename, 'r') as fid:
            success = 0
            data = json.load(fid)
            for obj in data:
                obj_id = obj['_id']
                url_endpoint = url + table + '/' + obj[sid] + '/' + stable
                res = req.delete(url_endpoint)
                if res.status_code == 200:
                    success = success + 1
            print('DELETE Table %s -- Success: %d of %d' % (stable, success, len(data)))

    print('Done!')

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('Please specify --import (-i) or --export (-e) or --delete (-d)')
    elif sys.argv[1] in ['-i', '--import']:
        import_data()
    elif sys.argv[1] in ['-e', '--export']:
        export_data()
    elif sys.argv[1] in ['-d', '--delete']:
        delete_data()


