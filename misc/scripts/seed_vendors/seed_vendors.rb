require 'rest-client'
require 'json'
require 'csv'

vendors = CSV.read('vendors.csv')
# vendors = CSV.read('test.csv')

vendors.each do |vendor|
  body = {
    name: vendor[0],
    password: vendor[1],
    description: vendor[2],
    state: vendor[3],
    city: vendor[4],
    address: vendor[5],
    coordinates: [vendor[6].to_f, vendor[7].to_f],
    email: vendor[8],
    type: vendor[9],
    phoneNumber: vendor[10],
    notiPref: vendor[11]
  }

  puts body.to_json
  response = RestClient.post 'https://ineed-db.mybluemix.net/api/vendors', body.to_json, content_type: :json
  puts response.code
end