require 'rest-client'
require 'json'

transactions = RestClient.get 'https://ineed-db.mybluemix.net/api/transactions'

transaction_ids = JSON.parse(transactions).map { |transaction| transaction["_id"] }

transactions_with_states = []

transaction_ids.each do |transaction_id|
  puts "Processing transaction #{transaction_id}"
  response = RestClient.get("https://ineed-db.mybluemix.net/api/transactions/#{transaction_id}/transaction_state"){|response, request, result| response }

  transactions_with_states.push(transaction_id) if response.code == 200
end

puts "Transactions with states\n"
puts transactions_with_states