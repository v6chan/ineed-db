require 'rest-client'
require 'json'

orders = RestClient.get 'https://ineed-db.mybluemix.net/api/orders'

order_ids = JSON.parse(orders).map { |order| order["_id"] }

orders_with_states = []

order_ids.each do |order_id|
  puts "Processing order #{order_id}"
  response = RestClient.get("https://ineed-db.mybluemix.net/api/orders/#{order_id}/order_state"){|response, request, result| response }

  orders_with_states.push(order_id) if response.code == 200
end

puts "Orders with states\n"
puts orders_with_states