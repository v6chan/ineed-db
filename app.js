var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var swagger = require('swagger-express');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// database connection info
if(process.env.VCAP_SERVICES) {
  var env = JSON.parse(process.env.VCAP_SERVICES);
  if (env['mongodb-2.4']) {
    var mongo = env['mongodb-2.4'][0]['credentials'];
    var bluemix_url = mongo.url;
  }
}

var local_database_name = 'ineed';
var local_database_uri  = 'mongodb://localhost/' + local_database_name
var database_uri = bluemix_url || local_database_uri
mongoose.connect(database_uri);

// ADD NEW ROUTES & ROUTE FILES HERE
var landing = require('./routes/index');
var members = require('./routes/api/members');
var deals = require('./routes/api/deals');
var vendors = require('./routes/api/vendors');
var items = require('./routes/api/items');
var proximity = require('./routes/api/proximity');
var orders = require('./routes/api/orders');
var transactions = require('./routes/api/transactions');
var ratings = require('./routes/api/ratings');
var sessions = require('./routes/api/sessions');

app.use('/', landing);
app.use('/api/members', members);
app.use('/api/deals', deals);
app.use('/api/vendors', vendors);
app.use('/api/items', items);
app.use('/api/proximity', proximity);
app.use('/api/orders', orders);
app.use('/api/transactions', transactions);
app.use('/api/ratings', ratings);
app.use('/api/sessions', sessions);

app.use('/docs', express.static('./public/swagger/'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers
// development error handler; will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler; no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;
