var Mongoose = require('mongoose');

var vendorSchema = new Mongoose.Schema({
  name:         { type: String, required: true },
  password:     { type: String, required: true },
  description:  { type: String, required: true },
  state:        { type: String, required: true },
  city:         { type: String, required: true },
  address:      { type: String, required: true },
  coordinates:  { type: [Number], index: '2d', required: true },
  email:        { type: String, required: true },
  type:         { type: String, required: true },
  phoneNumber:  { type: String, required: true },
  notiPref:     { type: String, required: true }
});

module.exports = Mongoose.model('Vendor', vendorSchema);

