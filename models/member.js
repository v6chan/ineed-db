var Mongoose = require('mongoose');

var memberSchema = new Mongoose.Schema({
  email:                { type: String, required: true },
  firstName:            { type: String, required: true },
  lastName:             { type: String, required: true },
  mobileNumber:         { type: String, required: true },
  categoryPreferences:  { type: [String] },
  proximityPreference:  { type: String },
  hashedPassword:       { type: String, required: true },
  creditCard:           { cardNumber:     { type: String },
                          cardCVC2:       { type: String },
                          cardExpiration: { type: String },
                          _id: false,
                          required: false
                        }
});

module.exports = Mongoose.model('Member', memberSchema);