var Mongoose = require('mongoose');
var Vendor = require('./vendor');

var itemSchema = new Mongoose.Schema({
  vendorId:     { type: Mongoose.Schema.Types.ObjectId, required: true, ref: 'Vendor'},
  prodName:     { type: String, required: true },
  prodDesc:     { type: String, required: true },
  quantity:     { type: Number, required: true },
  category:     { type: String, required: true },
  price:        { type: Number, required: true }
});

module.exports = Mongoose.model('Item', itemSchema);
