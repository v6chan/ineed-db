var Mongoose = require('mongoose');
var Order = require('./order');
var Vendor = require('./vendor');
var Deal = require('./deal');
var Item = require('./item');

var transactionSchema = new Mongoose.Schema({
  orderId:      { type: Mongoose.Schema.Types.ObjectId, required: true, ref: 'Order'},
  itemId:       { type: Mongoose.Schema.Types.ObjectId, ref: 'Item'},
  quantity:     { type: Number, required: true },
  unitPrice:    { type: Number, required: true },
  vendorId:     { type: Mongoose.Schema.Types.ObjectId, required: true, ref: 'Vendor'},
  dealId:       { type: Mongoose.Schema.Types.ObjectId, ref: 'Deal'},
  dealDiscount: { type: Number },
});

module.exports = Mongoose.model('Transaction', transactionSchema);
