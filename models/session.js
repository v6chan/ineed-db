var Mongoose = require('mongoose');

var sessionSchema = new Mongoose.Schema({
  memberEmail:      { type: String, required: true },
  sessionToken:     { type: String, required: true, unique: true },
  expirationDate:   { type: Date, required: true }
});

sessionSchema.path('expirationDate').expires('10s');

module.exports = Mongoose.model('Session', sessionSchema);
