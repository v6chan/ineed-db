var Mongoose = require('mongoose');
var Order = require('./order');

var orderStateSchema = new Mongoose.Schema({
  orderId:      { type: Mongoose.Schema.Types.ObjectId, required: true, ref: 'Order'},
  currentState: { type: Number, required: true }
});

module.exports = Mongoose.model('OrderState', orderStateSchema);