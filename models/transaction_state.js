var Mongoose = require('mongoose');
var Transaction = require('./transaction');

var transactionStateSchema = new Mongoose.Schema({
  transactionId:  { type: Mongoose.Schema.Types.ObjectId, required: true, ref: 'Transaction'},
  currentState:   { type: Number, required: true }
});

module.exports = Mongoose.model('TransactionState', transactionStateSchema);