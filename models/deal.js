var Mongoose = require('mongoose');
var Vendor = require('./vendor');
var Item = require('./item');

var dealSchema = new Mongoose.Schema({
  dealName:     { type: String, required: true },
  vendorName:   { type: String, required: true },
  vendorId:     { type: Mongoose.Schema.Types.ObjectId, required: true, ref: 'Vendor' },
  type:         { type: String, required: true },
  price:        { type: Number, required: true },
  discount:     { type: Number, required: true },
  expireDate:   { type: String, required: true },
  couponCode:   { type: String, required: true },
  itemSell:     { type: [{type: Mongoose.Schema.Types.ObjectId, ref: 'Item'}], required: true },
  redeemCount:  { type: Number, required: false, default: 0 },
  sendCount:    { type: Number, required: false, default: 0 }

});

module.exports = Mongoose.model('Deal', dealSchema);

