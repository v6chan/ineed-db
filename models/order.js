var Mongoose = require('mongoose');
var Deal = require('./deal');

var orderSchema = new Mongoose.Schema({
  paymentType:  { type: String, required: true },
  memberEmail:  { type: String, required: true },
  date:         { type: Date, default: Date.now },
  total:        { type: Number, required: true },
  tax:          { type: Number, required: true },
  dealId:       { type: Mongoose.Schema.Types.ObjectId, ref: 'Deal'},
  dealDiscount: { type: Number },
});

module.exports = Mongoose.model('Order', orderSchema);
