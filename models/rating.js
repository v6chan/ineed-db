var Mongoose = require('mongoose');
var Deal = require('./deal');

var stars = '1 2 3 4 5'.split(' ');

var ratingSchema = new Mongoose.Schema({
  dealId:       { type: Mongoose.Schema.Types.ObjectId, required: true, ref: 'Deal'},
  memberEmail:  { type: String, required: true },
  stars:        { type: String, enum: stars, required: true }
});

module.exports = Mongoose.model('Rating', ratingSchema);
