var mongoose = require('mongoose');

// connect to database
var local_database_name = 'ineed';
var local_database_uri  = 'mongodb://localhost/' + local_database_name
var database_uri = process.env.MONGOHQ_URL || local_database_uri
mongoose.connect(database_uri);

// Load in the items
var Item  = require('../../models/item');
var items = require('./items_seed.json');

Item.find().remove().exec(onceItemClear);

function onceItemClear(err) {
  if(err) console.log(err);

  var to_save_count = items.length;
  for(var i=0; i < items.length; i++) {
    var json = items[i];
    var item = new Item(json);
    
    var hex = String(i);
    while (hex.length < 24) {
        hex = "0" + hex;
    }

    item._id = mongoose.Types.ObjectId.createFromHexString(hex);

    item.save(function(err, item) {
      if(err) console.log(err);

      to_save_count--;
      console.log(to_save_count + ' left to save');

      if(to_save_count <= 0) {
        console.log('DONE');
        mongoose.connection.close()
      }
    });
  }
}
