var mongoose = require('mongoose');

// connect to database
var local_database_name = 'ineed';
var local_database_uri  = 'mongodb://localhost/' + local_database_name
var database_uri = process.env.MONGOHQ_URL || local_database_uri
mongoose.connect(database_uri);

// Load in the memebers
var Member  = require('../../models/member');
var members = require('./members_seed.json');

Member.find().remove().exec(onceClear);

function onceClear(err) {
  if(err) console.log(err);

  var to_save_count = members.length;
  for(var i=0; i < members.length; i++) {
    var json = members[i];
    var member = new Member(json);
    
    var hex = String(i);
    while (hex.length < 24) {
        hex = "0" + hex;
    }

    member._id = mongoose.Types.ObjectId.createFromHexString(hex);

    member.save(function(err, member) {
      if(err) console.log(err);

      to_save_count--;
      console.log(to_save_count + ' left to save');

      if(to_save_count <= 0) {
        console.log('DONE');
        mongoose.connection.close()
      }
    });
  }
}
