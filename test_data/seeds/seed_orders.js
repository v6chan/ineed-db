var mongoose = require('mongoose');

// connect to database
var local_database_name = 'ineed';
var local_database_uri  = 'mongodb://localhost/' + local_database_name
var database_uri = process.env.MONGOHQ_URL || local_database_uri
mongoose.connect(database_uri);

// Load in the orders
var Order      = require('../../models/order');
var OrderState = require('../../models/order_state');
var orders     = require('./orders_seed.json');

OrderState.find().remove(statesClear);

function statesClear(err) {
  if(err) console.log(err);
}

Order.find().remove().exec(onceClear);

function onceClear(err) {
  if(err) console.log(err);

  var to_save_count = orders.length;
  for(var i=0; i < orders.length; i++) {
    var json = orders[i];
    var order = new Order(json);
    
    var hex = String(i);
    while (hex.length < 24) {
        hex = "0" + hex;
    }

    order._id = mongoose.Types.ObjectId.createFromHexString(hex);

    order.save(function(err, order) {
      if(err) console.log(err);

      to_save_count--;
      console.log(to_save_count + ' left to save');

      if(to_save_count <= 0) {
        console.log('DONE');
        mongoose.connection.close()
      }
    });
  }
}
