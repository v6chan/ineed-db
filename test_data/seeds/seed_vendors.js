var mongoose = require('mongoose');

// connect to database
var local_database_name = 'ineed';
var local_database_uri  = 'mongodb://localhost/' + local_database_name
var database_uri = process.env.MONGOHQ_URL || local_database_uri
mongoose.connect(database_uri);

// Load in the vendors
var Vendor  = require('../../models/vendor');
var vendors = require('./vendors_seed.json');

Vendor.find().remove().exec(onceClear);

function onceClear(err) {
  if(err) console.log(err);

  var to_save_count = vendors.length;
  for(var i=0; i < vendors.length; i++) {
    var json = vendors[i];
    var vendor = new Vendor(json);
    
    var hex = String(i);
    while (hex.length < 24) {
        hex = "0" + hex;
    }

    vendor._id = mongoose.Types.ObjectId.createFromHexString(hex);

    vendor.save(function(err, vendor) {
      if(err) console.log(err);

      to_save_count--;
      console.log(to_save_count + ' left to save');

      if(to_save_count <= 0) {
        console.log('DONE');
        mongoose.connection.close()
      }
    });
  }
}
