var mongoose = require('mongoose');

// connect to database
var local_database_name = 'ineed';
var local_database_uri  = 'mongodb://localhost/' + local_database_name
var database_uri = process.env.MONGOHQ_URL || local_database_uri
mongoose.connect(database_uri);

// Load in the transactions
var Transaction      = require('../../models/transaction');
var TransactionState = require('../../models/transaction_state');
var transactions     = require('./transactions_seed.json');

TransactionState.find().remove(statesClear);

function statesClear(err) {
  if(err) console.log(err);
}

Transaction.find().remove().exec(onceClear);

function onceClear(err) {
  if(err) console.log(err);

  var to_save_count = transactions.length;
  for(var i=0; i < transactions.length; i++) {
    var json = transactions[i];
    var transaction = new Transaction(json);
    
    var hex = String(i);
    while (hex.length < 24) {
        hex = "0" + hex;
    }

    transaction._id = mongoose.Types.ObjectId.createFromHexString(hex);

    transaction.save(function(err, transaction) {
      if(err) console.log(err);

      to_save_count--;
      console.log(to_save_count + ' left to save');

      if(to_save_count <= 0) {
        console.log('DONE');
        mongoose.connection.close()
      }
    });
  }
}
