var request = require('supertest');
var should  = require('should');

var url = 'http://localhost:3000/api';

describe('Members', function() {

    describe('GET', function() {

        // standard get all
        it('should get all members', function(done) {
            request(url)
                .get('/members')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.length.should.equal(4);
                    done();
                });
        });

        // single query arg
        it('should get all members with specified field', function(done) {
            request(url)
                .get('/members?firstName=John')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.length.should.equal(3);
                    done();
                });
        });

        // unknown query arg
        it('should get no members due to unknown query arg', function(done) {
            request(url)
                .get('/members?foo=bar')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.length.should.equal(0);
                    done();
                });
        });

        // specific member id
        it('should get the specific member', function(done) {
            request(url)
                .get('/members/mcjohn@example.com')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.firstName.should.equal('John');
                    res.body.lastName.should.equal('McDonald');
                    done();
                });
        });

        // query on member email
        it('should get the specific member using email', function(done) {
            request(url)
                .get('/members?email=jsmith@example.com')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.length.should.equal(1);
                    res.body[0].firstName.should.equal('John');
                    res.body[0].lastName.should.equal('Smith');
                    done();
                });
        });

        // query on member lastName
        it('should get the specific member using lastName', function(done) {
            request(url)
                .get('/members?lastName=Smith')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.length.should.equal(1);
                    res.body[0].email.should.equal('jsmith@example.com');
                    done();
                });
        });

        // query on member mobile number
        it('should get the specific member using mobileNumber', function(done) {
            request(url)
                .get('/members?mobileNumber=546-731-8934')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.length.should.equal(1);
                    res.body[0].email.should.equal('j.stewart@cc.com')
                    done();
                });
        });

        // query on member proximityPreference
        it('should get the specific member using proximityPreference', function(done) {
            request(url)
                .get('/members?proximityPreference=example')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.length.should.equal(4);
                    done();
                });
        });

        // query on member firstName and proximityPreference
        it('should get set of members using firstName and proximityPreference', function(done) {
            request(url)
                .get('/members?proximityPreference=example&firstName=John')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.length.should.equal(3);
                    done();
                });
        });

        // query on member first and last name
        it('should get the specific member using first and last name', function(done) {
             request(url)
                 .get('/members?firstName=John&lastName=Smith')
                 .send()
                 .end(function (err, res) {
                     res.statusCode.should.equal(200);
                     res.body.length.should.equal(1);
                     res.body[0].firstName.should.equal('John');
                     res.body[0].lastName.should.equal('Smith');
                     done();
                 });
        });

        // unknown member id
        it('should fail from unknown member id', function(done) {
            request(url)
                .get('/members/111111111111111111111111')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(404);
                    done();
                });
        });

    });

    describe('POST', function() {

        // standard add member
        it('should add the member described', function(done) {
            var body = {
                "email": "unittest@test.com",
                "firstName": "Unit",
                "lastName": "Test",
                "mobileNumber": "123-123-1234",
                "hashedPassword": "2034921d061b158db3a620ed5fb1451d71d85b12e6fea8f063878535fc5e316b",
                "categoryPreferences": ["tests"],
                "proximityPreference": "example"
            };

            request(url)
                .post('/members')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    request(url)
                        .get('/members?firstName=Unit&lastName=Test')
                        .send()
                        .end(function (err, res) {
                            res.statusCode.should.equal(200);
                            res.body.length.should.equal(1);
                            res.body[0].email.should.equal('unittest@test.com');
                            done();
                        });
                });

        });

        it('should reject the member described with duplicate email', function(done) {
            var body = {
                "email": "unittest@test.com",
                "firstName": "IM",
                "lastName": "CRAZY",
                "mobileNumber": "123-123-1234",
                "hashedPassword": "2034921d061b158db3a620ed5fb1451d71d85b12e6fea8f063878535fc5e316b",
                "categoryPreferences": ["tests"],
                "proximityPreference": "example"
            };

            request(url)
                .post('/members')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(409);
                    done();
                });

        });

        // add member with improper json
        it('should reject the member described with incomplete data', function(done) {
            var body = {
                "firstName": "Unit"
            };

            request(url)
                .post('/members')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(400);
                    done();
                });
        });

    });

    describe('PUT', function() {

        // modify member
        it('should modify the member', function(done) {
            var body = {
                "firstName": "Old"
            };

            request(url)
                .put('/members/mcjohn@example.com')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    request(url)
                        .get('/members/mcjohn@example.com')
                        .send()
                        .end(function (err, res) {
                            res.statusCode.should.equal(200);
                            res.body.firstName.should.equal('Old');
                            res.body.lastName.should.equal('McDonald');
                            done();
                        });
                });
        });

        // unknown id
        it('should reject the modification to unknown member', function(done) {
            var body = {
                "firstName": "Old"
            };

            request(url)
                .put('/members/whosedis@example.com')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(404);
                    done();
                });
        });

        // unknwon field
        it('should not modify the member due to unknown property', function(done) {
            var body = {
                "foo": "bar"
            };

            request(url)
                .put('/members/mcjohn@example.com')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    request(url)
                        .get('/members/mcjohn@example.com')
                        .send()
                        .end(function (err, res) {
                            res.statusCode.should.equal(200);
                            res.body.firstName.should.equal('Old');
                            res.body.lastName.should.equal('McDonald');
                            res.body.hasOwnProperty('foo').should.equal(false);
                            done();
                        });
                });
        });

    });

    describe('DELETE', function() {

        // delete member
        it('should delete the member', function(done) {
            request(url)
                .delete('/members/mcjohn@example.com')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    request(url)
                        .get('/members/mcjohn@example.com')
                        .send()
                        .end(function (err, res) {
                            res.statusCode.should.equal(404);
                            done();
                        });
                });
        });

        // unknown id
        it('should fail to delete unknown member', function(done) {
            request(url)
                .delete('/members/111111111111111111111111')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(404);
                    done();
                });
        });

    });

}); 

