var request = require('supertest');
var should  = require('should');

var url = 'http://localhost:3000/api';

describe('Orders', function() {
    describe('GET', function() {
        it('should get all orders', function(done) {
            request(url)
                .get('/orders')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.length.should.equal(3);
                    done();
                });
        });

        it('should get all orders with specified field', function(done) {
            request(url)
                .get('/orders?paymentType=debit')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.length.should.equal(2);
                    done();
                });
        });

        it('should get no orders due to unknown query arg', function(done) {
            request(url)
                .get('/orders?foo=bar')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.length.should.equal(0);
                    done();
                });
        });

        it('should get the specific order', function(done) {
            request(url)
                .get('/orders/000000000000000000000002')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.total.should.equal(2.99);
                    res.body.memberEmail.should.equal('john@example.com');
                    done();
                });
        });

        it('should fail from unknown order id', function(done) {
            request(url)
                .get('/orders/111111111111111111111111')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(404);
                    done();
                });
        });

    });

    describe('POST', function() {
        it('should add the order described', function(done) {
            var body = {
                "paymentType": "paypal",
                "tax": 0.02,
                "total": 3.33,
                "memberEmail": "jane@example.com"
            };

            request(url)
                .post('/orders')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    request(url)
                        .get('/orders?memberEmail=jane@example.com&paymentType=paypal')
                        .send()
                        .end(function (err, res) {
                            res.statusCode.should.equal(200);
                            res.body.length.should.equal(1);
                            done();
                        });
                });

        });

        it('should reject the order described with incomplete data', function(done) {
            var body = {
                "paymentType": "debit"
            };

            request(url)
                .post('/orders')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(400);
                    done();
                });
        });

    });

    describe('PUT', function() {
        it('should modify the order', function(done) {
            var body = {
                "paymentType": "paypal"
            };

            request(url)
                .put('/orders/000000000000000000000001')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    request(url)
                        .get('/orders/000000000000000000000001')
                        .send()
                        .end(function (err, res) {
                            res.statusCode.should.equal(200);
                            res.body.memberEmail.should.equal('testerson@example.com');
                            res.body.paymentType.should.equal('paypal');
                            done();
                        });
                });
        });

        it('should reject the modification to unknown order', function(done) {
            var body = {
                "paymentType": "paypal"
            };

            request(url)
                .put('/orders/111111111111111111111111')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(404);
                    done();
                });
        });

        it('should not modify the order due with unknown property', function(done) {
            var body = {
                "foo": "bar"
            };

            request(url)
                .put('/orders/000000000000000000000001')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    request(url)
                        .get('/orders/000000000000000000000001')
                        .send()
                        .end(function (err, res) {
                            res.statusCode.should.equal(200);
                            res.body.memberEmail.should.equal('testerson@example.com');
                            res.body.paymentType.should.equal('paypal');
                            res.body.hasOwnProperty('foo').should.equal(false);
                            done();
                        });
                });
        });

    });

    describe('DELETE', function() {
        it('should delete the order', function(done) {
            request(url)
                .delete('/orders/000000000000000000000001')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    request(url)
                        .get('/orders/000000000000000000000001')
                        .send()
                        .end(function (err, res) {
                            res.statusCode.should.equal(404);
                            done();
                        });
                });
        });

        it('should fail to delete unknown order', function(done) {
            request(url)
                .delete('/orders/111111111111111111111111')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(404);
                    done();
                });
        });

    });

    describe('Orders State', function() {
        describe('POST', function() {
            it('should add the order state described', function(done) {
                var body = {
                  "currentState": 3
                };

                request(url)
                    .post('/orders/000000000000000000000000/order_state')
                    .send(body)
                    .end(function (err, res) {
                        res.statusCode.should.equal(200);
                        request(url)
                            .get('/orders/000000000000000000000000/order_state')
                            .send()
                            .end(function (err, res) {
                                res.statusCode.should.equal(200);
                                res.body.currentState.should.equal(3);
                                done();
                            });
                    });

            });
        });

        describe('GET', function() {
            it('should return the order state for specified order', function(done) {
                request(url)
                    .get('/orders/000000000000000000000000/order_state')
                    .send()
                    .end(function (err, res) {
                        res.statusCode.should.equal(200);
                        res.body.currentState.should.equal(3);
                        done();
                    });
            });
        });

        describe('PUT', function() {
            it('should modify the order state', function(done) {
                var body = {
                    "currentState": 2
                };

                request(url)
                    .put('/orders/000000000000000000000000/order_state')
                    .send(body)
                    .end(function (err, res) {
                        res.statusCode.should.equal(200);
                        request(url)
                            .get('/orders/000000000000000000000000/order_state')
                            .send()
                            .end(function (err, res) {
                                res.statusCode.should.equal(200);
                                res.body.currentState.should.equal(2);
                                done();
                            });
                    });
            });
        });

        describe('DELETE', function() {
            it('should delete the order state', function(done) {
                request(url)
                    .delete('/orders/000000000000000000000000/order_state')
                    .send()
                    .end(function (err, res) {
                        res.statusCode.should.equal(200);
                        request(url)
                            .get('/orders/000000000000000000000000/order_state')
                            .send()
                            .end(function (err, res) {
                                res.statusCode.should.equal(404);
                                done();
                            });
                    });
            });
        });
    });
}); 
