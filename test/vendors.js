var request = require('supertest');
var should  = require('should');

var url = 'http://localhost:3000/api';

describe('Vendors', function() {

    describe('GET', function() {

        // standard get all
        it('should get all vendors', function(done) {
            request(url)
                .get('/vendors')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.length.should.equal(3);
                    done();
                });
        });

        // single query arg
        it('should get all vendors with specified field', function(done) {
            request(url)
                .get('/vendors?name=beatles.com')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.length.should.equal(1);
                    res.body[0].email.should.equal('ringo@beatles.com')
                    done();
                });
        });

        // unknown query arg
        it('should get no vendors due to unknown query arg', function(done) {
            request(url)
                .get('/vendors?foo=bar')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.length.should.equal(0);
                    done();
                });
        });

        // specific vendor id
        it('should get the specific vendors', function(done) {
            request(url)
                .get('/vendors/000000000000000000000002')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.name.should.equal('BanzaiBurritos');
                    res.body.address.should.equal('321 Taco Lane');
                    done();
                });
        });

        // unknown vendor through id
        it('should fail from unknown vendor id', function(done) {
            request(url)
                .get('/vendors/111111111111111111111111')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(404);
                    done();
                });
        });

        // specific vendor through state
        it('should get vendor with specified state', function(done) {
            request(url)
                .get('/vendors?state=UK')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.length.should.equal(1);
                    res.body[0].email.should.equal('ringo@beatles.com')
                    done();
                });
        });

        // specific vendor through city
        it('should get vendor with specified city', function(done) {
            request(url)
                .get('/vendors?city=Liverpool')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.length.should.equal(1);
                    res.body[0].email.should.equal('ringo@beatles.com')
                    done();
                });
        });

        // specific vendor through address
        it('should get vendor with specified address', function(done) {
            request(url)
                .get('/vendors?address=8 Abbey Road')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.length.should.equal(1);
                    res.body[0].email.should.equal('ringo@beatles.com')
                    done();
                });
        });

        // query vendor through coordinates
        it('should get specific vendor according to coordinates', function(done) {
            request(url)
                .get('/vendors?coordinates=5&coordinates=91')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.length.should.equal(1);
                    res.body[0].name.should.equal("beatles.com")
                    done();
                });
        });


        // query vendor through type
        it('should get specific vendor according to type', function(done) {
            request(url)
                .get('/vendors?type=music')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.length.should.equal(1);
                    res.body[0].name.should.equal("beatles.com")
                    done();
                });
        });


        // query vendor through phoneNumber
        it('should get specific vendor according to phoneNumber', function(done) {
            request(url)
                .get('/vendors?phoneNumber=487-391-8371')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.length.should.equal(1);
                    res.body[0].name.should.equal("beatles.com")
                    done();
                });
        });


        // query vendor through notification preference
        it('should get specific vendor according to notification preference', function(done) {
            request(url)
                .get('/vendors?notiPref=phone')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.length.should.equal(1);
                    res.body[0].name.should.equal("beatles.com")
                    done();
                });
        });

    });

    describe('POST', function() {

        // standard add
        it('should add the vendor described', function(done) {
            var body = {
                "name": "TestVendor",
                "password": "ilovecats",
                "description": "nothing really to say...",
                "state": "CA",
                "city": "La Jolla",
                "address": "1234 Test Drive",
                "coordinates": [42, 42],
                "email": "unittest@test.com",
                "type": "software",
                "phoneNumber": "987-654-3210",
                "notiPref": "email"
            };

            request(url)
                .post('/vendors')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    request(url)
                        .get('/vendors?name=TestVendor&type=software')
                        .send()
                        .end(function (err, res) {
                            res.statusCode.should.equal(200);
                            res.body.length.should.equal(1);
                            res.body[0].state.should.equal('CA')
                            done();
                        });
                });

        });

        // add vendor with improper json
        it('should reject the vendor described with incomplete data', function(done) {
            var body = {
                "name": "Empty Warehouse"
            };

            request(url)
                .post('/vendors')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(400);
                    done();
                });
        });

    });

    describe('PUT', function() {

        // modify vendor
        it('should modify the vendor', function(done) {
            var body = {
                "name": "eagles.com"
            };

            request(url)
                .put('/vendors/000000000000000000000001')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    request(url)
                        .get('/vendors/000000000000000000000001')
                        .send()
                        .end(function (err, res) {
                            res.statusCode.should.equal(200);
                            res.body.name.should.equal('eagles.com');
                            res.body.address.should.equal('8 Abbey Road');
                            done();
                        });
                });
        });

        // unknown id
        it('should reject the modification to unknown vendor', function(done) {
            var body = {
                "name": "foobar"
            };

            request(url)
                .put('/vendors/111111111111111111111111')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(404);
                    done();
                });
        });

        // unknwon field
        it('should not modify the vendor due with unknown property', function(done) {
            var body = {
                "foo": "bar"
            };

            request(url)
                .put('/vendors/000000000000000000000001')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    request(url)
                        .get('/vendors/000000000000000000000001')
                        .send()
                        .end(function (err, res) {
                            res.statusCode.should.equal(200);
                            res.body.name.should.equal('eagles.com');
                            res.body.address.should.equal('8 Abbey Road');
                            res.body.hasOwnProperty('foo').should.equal(false);
                            done();
                        });
                });
        });

    });

    describe('DELETE', function() {

        // delete vendor
        it('should delete the vendor', function(done) {
            request(url)
                .delete('/vendors/000000000000000000000001')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    request(url)
                        .get('/vendors/000000000000000000000001')
                        .send()
                        .end(function (err, res) {
                            res.statusCode.should.equal(404);
                            done();
                        });
                });
        });

        // unknown id
        it('should fail to delete unknown vendor', function(done) {
            request(url)
                .delete('/vendors/111111111111111111111111')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(404);
                    done();
                });
        });

    });

}); 

