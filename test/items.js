var request = require('supertest');
var should  = require('should');

var url = 'http://localhost:3000/api';

describe('Items', function() {

    describe('GET', function() {

        // standard get all
        it('should get all items', function(done) {
            request(url)
                .get('/items')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.length.should.equal(4);
                    done();
                });
        });

        // single query arg
        it('should get all items with specified field', function(done) {
            request(url)
                .get('/items?prodName=apple')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.length.should.equal(2);
                    done();
                });
        });

        // unknown query arg
        it('should get no items due to unknown query arg', function(done) {
            request(url)
                .get('/items?foo=bar')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.length.should.equal(0);
                    done();
                });
        });

        // specific item id
        it('should get the specific item', function(done) {
            request(url)
                .get('/items/000000000000000000000001')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.prodName.should.equal('banana');
                    res.body.price.should.equal(1.3);
                    done();
                });
        });

        // unknown item id
        it('should fail from unknown item id', function(done) {
            request(url)
                .get('/items/111111111111111111111111')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(404);
                    done();
                });
        });

    });

    describe('POST', function() {

        // standard add
        it('should add the item described', function(done) {
            var body = {
                "vendorId": "000000000000000000000042",
                "prodName": "TestItem",
                "prodDesc": "TestItem Description",
                "category": "garbage",
                "quantity": 5,
                "price": 100.00
            };

            request(url)
                .post('/items')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    request(url)
                        .get('/items?prodName=TestItem&vendorId=000000000000000000000042')
                        .send()
                        .end(function (err, res) {
                            res.statusCode.should.equal(200);
                            res.body.length.should.equal(1);
                            done();
                        });
                });

        });

        // add item with improper json
        it('should reject the item described with incomplete data', function(done) {
            var body = {
                "prodName": "gold bar"
            };

            request(url)
                .post('/items')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(400);
                    done();
                });
        });

    });

    describe('PUT', function() {

        // modify item
        it('should modify the item', function(done) {
            var body = {
                "prodName": "peach"
            };

            request(url)
                .put('/items/000000000000000000000002')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    request(url)
                        .get('/items/000000000000000000000002')
                        .send()
                        .end(function (err, res) {
                            res.statusCode.should.equal(200);
                            res.body.prodName.should.equal('peach');
                            res.body.vendorId.should.equal('000000000000000000000002');
                            done();
                        });
                });
        });

        // unknown id
        it('should reject the modification to unknown item', function(done) {
            var body = {
                "name": "foobar"
            };

            request(url)
                .put('/items/111111111111111111111111')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(404);
                    done();
                });
        });

        // unknwon field
        it('should not modify the item due with unknown property', function(done) {
            var body = {
                "foo": "bar"
            };

            request(url)
                .put('/items/000000000000000000000002')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    request(url)
                        .get('/items/000000000000000000000002')
                        .send()
                        .end(function (err, res) {
                            res.statusCode.should.equal(200);
                            res.body.prodName.should.equal('peach');
                            res.body.vendorId.should.equal('000000000000000000000002');
                            res.body.hasOwnProperty('foo').should.equal(false);
                            done();
                        });
                });
        });

    });

    describe('DELETE', function() {

        // delete item
        it('should delete the item', function(done) {
            request(url)
                .delete('/items/000000000000000000000001')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    request(url)
                        .get('/items/000000000000000000000001')
                        .send()
                        .end(function (err, res) {
                            res.statusCode.should.equal(404);
                            done();
                        });
                });
        });

        // unknown id
        it('should fail to delete unknown item', function(done) {
            request(url)
                .delete('/items/111111111111111111111111')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(404);
                    done();
                });
        });

    });

}); 

