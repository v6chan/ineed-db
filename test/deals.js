var request = require('supertest');
var should  = require('should');

var url = 'http://localhost:3000/api';

describe('Deals', function() {

    describe('GET', function() {

        // standard get all
        it('should get all Deals', function(done) {
            request(url)
                .get('/deals')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.length.should.equal(3);
                    done();
                });
        });

        // single query arg
        it('should get all deals with specified field', function(done) {
            request(url)
                .get('/deals?vendorName=mystore.com')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.length.should.equal(2);
                    done();
                });
        });

        // unknown query arg
        it('should get no deals due to unknown query arg', function(done) {
            request(url)
                .get('/deals?foo=bar')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.length.should.equal(0);
                    done();
                });
        });

        // specific deal id
        it('should get the specific deals', function(done) {
            request(url)
                .get('/deals/000000000000000000000002')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.dealName.should.equal('deal1');
                    res.body.vendorName.should.equal('yourstore.com');
                    done();
                });
        });

        // unknown deal id
        it('should fail from unknown deal id', function(done) {
            request(url)
                .get('/deals/111111111111111111111111')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(404);
                    done();
                });
        });

    });

    describe('POST', function() {

        // standard add
        it('should add the deal described', function(done) {
            var body = {
                "dealName": "TestDeal",
                "vendorName": "test.unit.edu",
                "vendorId": "000000000000000000001234",
                "type": "software",
                "price": 1.00,
                "discount": 0.00,
                "expireDate": "Jan 1, 1970",
                "couponCode": "efg",
                "itemSell": ["000000000000000000000004"]
            };

            request(url)
                .post('/deals')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    request(url)
                        .get('/deals?dealName=TestDeal&vendorName=test.unit.edu')
                        .send()
                        .end(function (err, res) {
                            res.statusCode.should.equal(200);
                            res.body.length.should.equal(1);
                            done();
                        });
                });

        });

        // add deal with improper json
        it('should reject the deal described with incomplete data', function(done) {
            var body = {
                "dealName": "half off"
            };

            request(url)
                .post('/deals')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(400);
                    done();
                });
        });

    });

    describe('PUT', function() {

        // modify deal
        it('should modify the deal', function(done) {
            var body = {
                "dealName": "hackedDeal"
            };

            request(url)
                .put('/deals/000000000000000000000001')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    request(url)
                        .get('/deals/000000000000000000000001')
                        .send()
                        .end(function (err, res) {
                            res.statusCode.should.equal(200);
                            res.body.dealName.should.equal('hackedDeal');
                            res.body.type.should.equal('movies');
                            done();
                        });
                });
        });

        // unknown id
        it('should reject the modification to unknown deal', function(done) {
            var body = {
                "name": "foobar"
            };

            request(url)
                .put('/deals/111111111111111111111111')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(404);
                    done();
                });
        });

        // unknwon field
        it('should not modify the deal due with unknown property', function(done) {
            var body = {
                "foo": "bar"
            };

            request(url)
                .put('/deals/000000000000000000000001')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    request(url)
                        .get('/deals/000000000000000000000001')
                        .send()
                        .end(function (err, res) {
                            res.statusCode.should.equal(200);
                            res.body.dealName.should.equal('hackedDeal');
                            res.body.type.should.equal('movies');
                            res.body.hasOwnProperty('foo').should.equal(false);
                            done();
                        });
                });
        });

    });

    describe('DELETE', function() {

        // delete deal
        it('should delete the deal', function(done) {
            request(url)
                .delete('/deals/000000000000000000000001')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    request(url)
                        .get('/deals/000000000000000000000001')
                        .send()
                        .end(function (err, res) {
                            res.statusCode.should.equal(404);
                            done();
                        });
                });
        });

        // unknown id
        it('should fail to delete unknown deal', function(done) {
            request(url)
                .delete('/deals/111111111111111111111111')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(404);
                    done();
                });
        });

    });

}); 

