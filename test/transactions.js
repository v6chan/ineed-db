var request = require('supertest');
var should  = require('should');

var url = 'http://localhost:3000/api';

describe('Transactions', function() {
    describe('GET', function() {
        it('should get all transactions', function(done) {
            request(url)
                .get('/transactions')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.length.should.equal(3);
                    done();
                });
        });

        it('should get all transactions with specified field', function(done) {
            request(url)
                .get('/transactions?orderId=55465be1aa2b104525207007')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.length.should.equal(2);
                    done();
                });
        });

        it('should get no transactions due to unknown query arg', function(done) {
            request(url)
                .get('/transactions?foo=bar')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.length.should.equal(0);
                    done();
                });
        });

        it('should get the specific transaction', function(done) {
            request(url)
                .get('/transactions/000000000000000000000002')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    res.body.quantity.should.equal(1);
                    res.body.vendorId.should.equal('999c40993d5ce8cc1e455bd4');
                    done();
                });
        });

        it('should fail from unknown transaction id', function(done) {
            request(url)
                .get('/transactions/111111111111111111111111')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(404);
                    done();
                });
        });

    });

    describe('POST', function() {
        it('should add the transaction described', function(done) {
            var body = {
                "orderId": "111111111111111111111111",
                "itemId": "123456789011111111111111",
                "quantity": 1,
                "unitPrice": 39.99,
                "vendorId": "553c40993d5ce8cc1e455bd4",
                "dealId": "553dd674641ddfd849dae1ea",
                "dealDiscount": 0.5
            };

            request(url)
                .post('/transactions')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    request(url)
                        .get('/transactions?vendorId=553c40993d5ce8cc1e455bd4&orderId=111111111111111111111111')
                        .send()
                        .end(function (err, res) {
                            res.statusCode.should.equal(200);
                            res.body.length.should.equal(1);
                            done();
                        });
                });

        });

        it('should add the transaction without itemId or dealId as described', function(done) {
            var body = {
                "orderId": "111111111111111111111222",
                "quantity": 1,
                "unitPrice": 99.99,
                "vendorId": "553c40993d5ce8cc1eabc666"
            };

            request(url)
                .post('/transactions')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    request(url)
                        .get('/transactions?vendorId=553c40993d5ce8cc1eabc666&orderId=111111111111111111111222')
                        .send()
                        .end(function (err, res) {
                            res.statusCode.should.equal(200);
                            res.body.length.should.equal(1);
                            done();
                        });
                });

        });

        it('should reject the transaction described with incomplete data', function(done) {
            var body = {
                "dealId": "553dd674641ddfd849dae1ea"
            };

            request(url)
                .post('/transactions')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(400);
                    done();
                });
        });

    });

    describe('PUT', function() {
        it('should modify the transaction', function(done) {
            var body = {
                "itemId": "12345ce1ab2b1c4525123aaa"
            };

            request(url)
                .put('/transactions/000000000000000000000001')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    request(url)
                        .get('/transactions/000000000000000000000001')
                        .send()
                        .end(function (err, res) {
                            res.statusCode.should.equal(200);
                            res.body.itemId.should.equal('12345ce1ab2b1c4525123aaa');
                            res.body.quantity.should.equal(3);
                            done();
                        });
                });
        });

        it('should reject the modification to unknown transaction', function(done) {
            var body = {
                "itemId": "12345ce1ab2b1c4525123aaa"
            };

            request(url)
                .put('/transactions/111111111111111111111111')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(404);
                    done();
                });
        });

        it('should not modify the transaction due to unknown property', function(done) {
            var body = {
                "foo": "bar"
            };

            request(url)
                .put('/transactions/000000000000000000000001')
                .send(body)
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    request(url)
                        .get('/transactions/000000000000000000000001')
                        .send()
                        .end(function (err, res) {
                            res.statusCode.should.equal(200);
                            res.body.itemId.should.equal('12345ce1ab2b1c4525123aaa');
                            res.body.quantity.should.equal(3);
                            res.body.hasOwnProperty('foo').should.equal(false);
                            done();
                        });
                });
        });

    });

    describe('DELETE', function() {
        it('should delete the transaction', function(done) {
            request(url)
                .delete('/transactions/000000000000000000000001')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(200);
                    request(url)
                        .get('/transactions/000000000000000000000001')
                        .send()
                        .end(function (err, res) {
                            res.statusCode.should.equal(404);
                            done();
                        });
                });
        });

        it('should fail to delete unknown transaction', function(done) {
            request(url)
                .delete('/transactions/111111111111111111111111')
                .send()
                .end(function (err, res) {
                    res.statusCode.should.equal(404);
                    done();
                });
        });

    });

    describe('Transactions State', function() {
        describe('POST', function() {
            it('should add the transaction state described', function(done) {
                var body = {
                    "currentState": 9
                };

                request(url)
                    .post('/transactions/000000000000000000000000/transaction_state')
                    .send(body)
                    .end(function (err, res) {
                        res.statusCode.should.equal(200);
                        request(url)
                            .get('/transactions/000000000000000000000000/transaction_state')
                            .send()
                            .end(function (err, res) {
                                res.statusCode.should.equal(200);
                                res.body.currentState.should.equal(9);
                                done();
                            });
                    });

            });
        });

        describe('GET', function() {
            it('should return the transaction state for specified transaction', function(done) {
                request(url)
                    .get('/transactions/000000000000000000000000/transaction_state')
                    .send()
                    .end(function (err, res) {
                        res.statusCode.should.equal(200);
                        res.body.currentState.should.equal(9);
                        done();
                    });
            });
        });

        describe('PUT', function() {
            it('should modify the transaction state', function(done) {
                var body = {
                    "currentState": 5
                };

                request(url)
                    .put('/transactions/000000000000000000000000/transaction_state')
                    .send(body)
                    .end(function (err, res) {
                        res.statusCode.should.equal(200);
                        request(url)
                            .get('/transactions/000000000000000000000000/transaction_state')
                            .send()
                            .end(function (err, res) {
                                res.statusCode.should.equal(200);
                                res.body.currentState.should.equal(5);
                                done();
                            });
                    });
            });
        });

        describe('DELETE', function() {
            it('should delete the transaction state', function(done) {
                request(url)
                    .delete('/transactions/000000000000000000000000/transaction_state')
                    .send()
                    .end(function (err, res) {
                        res.statusCode.should.equal(200);
                        request(url)
                            .get('/transactions/000000000000000000000000/transaction_state')
                            .send()
                            .end(function (err, res) {
                                res.statusCode.should.equal(404);
                                done();
                            });
                    });
            });
        });
    });
}); 
