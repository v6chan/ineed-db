var mongoose = require('mongoose')
var express = require('express');
var router = express.Router();
var Item = require('../../models/item');

// GET Requests
router.get('/', function(req, res) {
  var filter = req.query;

  Item.find(filter).exec(response);

  function response(err, items) {
    res.json(items);
  }
});

router.get('/:id', function(req, res) {
  var id = req.param('id');

  Item.findById(id).exec(response);

  function response(err, item) {
    if(item) {
      res.json(item);
    }
    else {
      res.status(404).end();
    }
  }
});

// POST Requests
router.post('/', function(req, res) {
  item = new Item({
    vendorId:   req.body.vendorId,
    prodName:   req.body.prodName,
    prodDesc:   req.body.prodDesc,
    quantity:   req.body.quantity,
    category:   req.body.category,
    price:      req.body.price,
  });

  if (req.body.hasOwnProperty('_id')) {
      item._id = mongoose.Types.ObjectId.createFromHexString(String(req.body._id));
  }

  item.save(function(err, item) {
    if(err) {
      res.status(400).end();
    }
    else {
      res.json(item);
    }
  });
});

// PUT Requests
router.put('/:id', function(req, res) {
  var id = req.param('id');

  Item.findById(id).exec(response);

  function response(err, item) {
    if(item) {
      item.vendorId     = (req.body.vendorId || item.vendorId)
      item.prodName     = (req.body.prodName || item.prodName)
      item.prodDesc     = (req.body.prodDesc || item.prodDesc)
      item.quantity     = (req.body.quantity || item.quantity)
      item.category     = (req.body.category || item.category)
      item.price        = (req.body.price || item.price)

      item.save(function (err) {
        if(err) {
          res.status(500).end();
        } 
        else {
          res.status(200).end();
        }
      });
    }
    else {
      res.status(404).end();
    }
  }
});

// DELETE Requests

router.delete('/:id', function(req, res) {
  var id = req.param('id');

  Item.findById(id).exec(response);

  function response(err, item) {
    if(item) {
      item.remove(function (err) {
        if(err) {
          res.status(500).end();
        } 
        else {
          res.status(200).end();
        }
      });
    }
    else {
      res.status(404).end();
    }
  }
});

module.exports = router;
