var mongoose = require('mongoose')
var express = require('express');
var router = express.Router();
var Transaction = require('../../models/transaction');
var TransactionState = require('../../models/transaction_state');

router.get('/', function(req, res) {
  var filter = req.query;

  Transaction.find(filter).exec(response);

  function response(err, transactions) {
    res.json(transactions);
  }
});

router.get('/:id', function(req, res) {
  var id = req.param('id');

  Transaction.findById(id).exec(response);

  function response(err, transaction) {
    if(transaction) {
      res.json(transaction);
    }
    else {
      res.status(404).end();
    }
  }
});

router.post('/', function(req, res) {
  transaction = new Transaction({
    orderId:      req.body.orderId,
    itemId:       req.body.itemId,
    quantity:     req.body.quantity,
    unitPrice:    req.body.unitPrice,
    vendorId:     req.body.vendorId,
    dealId:       req.body.dealId,
    dealDiscount: req.body.dealDiscount
  });

  if (req.body.hasOwnProperty('_id')) {
      transaction._id = mongoose.Types.ObjectId.createFromHexString(String(req.body._id));
  }

  transaction.save(function(err, transaction) {
    if(err) {
      res.status(400).end();
    }
    else {
      res.json(transaction);
    }
  });
});

router.put('/:id', function(req, res) {
  var id = req.param('id');

  Transaction.findById(id).exec(response);

  function response(err, transaction) {
    if(transaction) {
      transaction.orderId      = (req.body.orderId || transaction.orderId),
      transaction.itemId       = (req.body.itemId || transaction.itemId),
      transaction.quantity     = (req.body.quantity || transaction.quantity),
      transaction.unitPrice    = (req.body.unitPrice || transaction.unitPrice),
      transaction.vendorId     = (req.body.vendorId || transaction.vendorId),
      transaction.dealId       = (req.body.dealId || transaction.dealId),
      transaction.dealDiscount = (req.body.dealDiscount || transaction.dealDiscount)

      transaction.save(function (err) {
        if(err) {
          res.status(500).end();
        } 
        else {
          res.status(200).end();
        }
      });
    }
    else {
      res.status(404).end();
    }
  }
});

router.delete('/:id', function(req, res) {
  var id = req.param('id');

  Transaction.findById(id).exec(response);

  function response(err, transaction) {
    if(transaction) {
      transaction.remove(function (err) {
        if(err) {
          res.status(500).end();
        } 
        else {
          res.status(200).end();
        }
      });
    }
    else {
      res.status(404).end();
    }
  }
});

router.get('/:id/transaction_state', function(req, res) {
  var transaction_id = req.param('id');

  if (transaction_id == '*') {
    TransactionState.find().exec(response);
  } else {
    TransactionState.findOne({transactionId: transaction_id}).exec(response);
  }

  function response(err, transactionState) {
    if(transactionState) {
      res.json(transactionState);
    }
    else {
      res.status(404).end();
    }
  }
});

router.post('/:id/transaction_state', function(req, res) {
  var transaction_id = req.param('id');

  newTransactionState = new TransactionState({
    transactionId: transaction_id,
    currentState:  req.body.currentState
  });

  if (req.body.hasOwnProperty('_id')) {
      newTransactionState._id = mongoose.Types.ObjectId.createFromHexString(String(req.body._id));
  }

  Transaction.findById(transaction_id).exec(create);

  function create(err, transaction) {
    if(transaction) {
      TransactionState.findOne({transactionId: transaction_id}).exec(response);

      function response(err, transactionState) {
        if(transactionState) {
          res.status(409).end();
        }
        else {
          newTransactionState.save(function(err, newTransactionState) {
            if(err) {
              res.status(400).end();
            }
            else {
              res.json(newTransactionState);
            }
          });
        }
      }
    }
    else {
      res.status(404).end();
    }
  }
});

router.put('/:id/transaction_state', function(req, res) {
  var transaction_id = req.param('id');

  TransactionState.findOne({transactionId: transaction_id}).exec(response);

  function response(err, transactionState) {
    if(transactionState) {
      transactionState.currentState = (req.body.currentState || transactionState.currentState)

      transactionState.save(function (err) {
        if(err) {
          res.status(500).end();
        } 
        else {
          res.status(200).end();
        }
      });
    }
    else {
      res.status(404).end();
    }
  }
});

router.delete('/:id/transaction_state', function(req, res) {
  var transaction_id = req.param('id');

  TransactionState.findOne({transactionId: transaction_id}).exec(response);

  function response(err, transactionState) {
    if(transactionState) {
      transactionState.remove(function (err) {
        if(err) {
          res.status(500).end();
        } 
        else {
          res.status(200).end();
        }
      });
    }
    else {
      res.status(404).end();
    }
  }
});

module.exports = router;
