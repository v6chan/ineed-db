var mongoose = require('mongoose')
var express = require('express');
var router = express.Router();
var Deal = require('../../models/deal');


// GET ALL Request (filter by query args)
router.get('/', function(req, res) {
  var filter = req.query;

  Deal.find(filter).exec(response);

  function response(err, deals) {
    res.json(deals);
  }
});


// GET SINGLE Request (by deal id)
router.get('/:id', function(req, res) {
  var id = req.param('id');

  Deal.findById(id).exec(response);

  function response(err, deal) {
    if(deal) {
      res.json(deal);
    }
    else {
      res.status(404).end();
    }
  }
});


// POST Requests
router.post('/', function(req, res) {
  deal = new Deal({
    dealName:    req.body.dealName,
    vendorName:  req.body.vendorName,
    vendorId:    req.body.vendorId,
    type:        req.body.type,
    price:       req.body.price,
    discount:    req.body.discount,
    expireDate:  req.body.expireDate,
    couponCode:  req.body.couponCode,
    itemSell:    req.body.itemSell,
    redeemCount: req.body.redeemCount,
    sendCount:   req.body.sendCount
  });

  if (req.body.hasOwnProperty('_id')) {
      deal._id = mongoose.Types.ObjectId.createFromHexString(String(req.body._id));
  }

  deal.save(function(err, deal) {
    if(err) {
      res.status(400).end();
    }
    else {
      res.json(deal);
    }
  });
});


// PUT Requests
router.put('/:id', function(req, res) {
  var id = req.param('id');

  Deal.findById(id).exec(response);

  function response(err, deal) {
    if(deal) {
      deal.dealName     = (req.body.dealName || deal.dealName)
      deal.vendorName   = (req.body.vendorName || deal.vendorName)
      deal.vendorId     = (req.body.vendorId || deal.vendorId)
      deal.type         = (req.body.type || deal.type)
      deal.price        = (req.body.price || deal.price)
      deal.discount     = (req.body.discount || deal.discount)
      deal.expireDate   = (req.body.expireDate || deal.expireDate)
      deal.couponCode   = (req.body.couponCode || deal.couponCode)
      deal.itemSell     = (req.body.itemSell || deal.itemSell)
      deal.redeemCount  = (req.body.redeemCount || deal.redeemCount)
      deal.sendCount    = (req.body.sendCount || deal.sendCount)

      deal.save(function (err) {
        if(err) {
          res.status(500).end();
        } 
        else {
          res.status(200).end();
        }
      });
    }
    else {
      res.status(404).end();
    }
  }
});


// DELETE Requests
router.delete('/:id', function(req, res) {
  var id = req.param('id');

  Deal.findById(id).exec(response);

  function response(err, deal) {
    if(deal) {
      deal.remove(function (err) {
        if(err) {
          res.status(500).end();
        } 
        else {
          res.status(200).end();
        }
      });
    }
    else {
      res.status(404).end();
    }
  }
});


module.exports = router;

