var mongoose = require('mongoose')
var express = require('express');
var router = express.Router();
var Rating = require('../../models/rating');

router.get('/', function(req, res) {
  var filter = req.query;

  Rating.find(filter).exec(response);

  function response(err, ratings) {
    res.json(ratings);
  }
});

router.get('/:id', function(req, res) {
  var id = req.param('id');

  Rating.findById(id).exec(response);

  function response(err, rating) {
    if(rating) {
      res.json(rating);
    }
    else {
      res.status(404).end();
    }
  }
});

router.post('/', function(req, res) {
  rating = new Rating({
    dealId:       req.body.dealId,
    memberEmail:  req.body.memberEmail,
    stars:        req.body.stars
  });

  if (req.body.hasOwnProperty('_id')) {
      rating._id = mongoose.Types.ObjectId.createFromHexString(String(req.body._id));
  }

  rating.save(function(err, rating) {
    if(err) {
      res.status(400).end();
    }
    else {
      res.json(rating);
    }
  });
});

router.put('/:id', function(req, res) {
  var id = req.param('id');

  Rating.findById(id).exec(response);

  function response(err, rating) {
    if(rating) {
      rating.dealId       = (req.body.dealId || rating.dealId)
      rating.memberEmail  = (req.body.memberEmail || rating.memberEmail)
      rating.stars        = (req.body.stars || rating.stars)

      rating.save(function (err) {
        if(err) {
          res.status(500).end();
        } 
        else {
          res.status(200).end();
        }
      });
    }
    else {
      res.status(404).end();
    }
  }
});

router.delete('/:id', function(req, res) {
  var id = req.param('id');

  Rating.findById(id).exec(response);

  function response(err, rating) {
    if(rating) {
      rating.remove(function (err) {
        if(err) {
          res.status(500).end();
        } 
        else {
          res.status(200).end();
        }
      });
    }
    else {
      res.status(404).end();
    }
  }
});

module.exports = router;
