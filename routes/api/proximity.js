var express = require('express');
var router = express.Router();
var Vendor = require('../../models/vendor');

router.get('/', function(req, res) {
  var coordinates = [];
  coordinates[0] = req.query.longitude;
  coordinates[1] = req.query.latitude;

  var distance = req.query.distance; // units = miles
  distance /= 3959; // converts miles to radians

  Vendor.find({
    coordinates: {
      $within: {
        $centerSphere: [coordinates, distance]
      }
    }
  }).exec(response);

  function response(err, vendors) {
    res.json(vendors);
  }
});

module.exports = router;