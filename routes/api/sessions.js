var mongoose = require('mongoose')
var express = require('express');
var router = express.Router();
var Session = require('../../models/session');

router.get('/', function(req, res) {
  var filter = req.query;

  Session.find(filter).exec(response);

  function response(err, sessions) {
    res.json(sessions);
  }
});

router.get('/:token', function(req, res) {
  var token = req.param('token');

  Session.findOne({ sessionToken: token }).exec(response);

  function response(err, session) {
    if(session) {
      res.json(session);
    }
    else {
      res.status(404).end();
    }
  }
});

router.post('/', function(req, res) {
  session = new Session({
    memberEmail:    req.body.memberEmail,
    sessionToken:   req.body.sessionToken,
    expirationDate: req.body.expirationDate
  });

  if (req.body.hasOwnProperty('_id')) {
      session._id = mongoose.Types.ObjectId.createFromHexString(String(req.body._id));
  }

  session.save(function(err, session) {
    if(err) {
      res.status(400).end();
    }
    else {
      res.json(session);
    }
  });
});

router.delete('/:token', function(req, res) {
  var token = req.param('token');

  Session.findOne({ sessionToken: token }).exec(response);

  function response(err, session) {
    if(session) {
      session.remove(function (err) {
        if(err) {
          res.status(500).end();
        } 
        else {
          res.status(200).end();
        }
      });
    }
    else {
      res.status(404).end();
    }
  }
});

module.exports = router;
