var mongoose = require('mongoose')
var express = require('express');
var router = express.Router();
var Order = require('../../models/order');
var OrderState = require('../../models/order_state');

router.get('/', function(req, res) {
  var filter = req.query;

  Order.find(filter).exec(response);

  function response(err, orders) {
    if(err) console.log(err);
    res.json(orders);
  }
});

router.get('/:id', function(req, res) {
  var id = req.param('id');

  Order.findById(id).exec(response);

  function response(err, order) {
    if(order) {
      res.json(order);
    }
    else {
      res.status(404).end();
    }
  }
});

router.post('/', function(req, res) {
  order = new Order({
    paymentType:  req.body.paymentType,
    memberEmail:  req.body.memberEmail,
    date:         req.body.date,
    total:        req.body.total,
    tax:          req.body.tax,
    dealId:       req.body.dealId,
    dealDiscount: req.body.dealDiscount
  });

  if (req.body.hasOwnProperty('_id')) {
      order._id = mongoose.Types.ObjectId.createFromHexString(String(req.body._id));
  }

  order.save(function(err, order) {
    if(err) {
      res.status(400).end();
    }
    else {
      res.json(order);
    }
  });
});

router.put('/:id', function(req, res) {
  var id = req.param('id');

  Order.findById(id).exec(response);

  function response(err, order) {
    if(order) {
      order.paymentType  = (req.body.paymentType || order.paymentType),
      order.memberEmail  = (req.body.memberEmail || order.memberEmail),
      order.date         = (req.body.date || order.date),
      order.total        = (req.body.total || order.total),
      order.tax          = (req.body.tax || order.tax),
      order.dealId       = (req.body.dealId || order.dealId),
      order.dealDiscount = (req.body.dealDiscount || order.dealDiscount)

      order.save(function (err) {
        if(err) {
          res.status(500).end();
        } 
        else {
          res.status(200).end();
        }
      });
    }
    else {
      res.status(404).end();
    }
  }
});

router.delete('/:id', function(req, res) {
  var id = req.param('id');

  Order.findById(id).exec(response);

  function response(err, order) {
    if(order) {
      order.remove(function (err) {
        if(err) {
          res.status(500).end();
        } 
        else {
          res.status(200).end();
        }
      });
    }
    else {
      res.status(404).end();
    }
  }
});

router.get('/:id/order_state', function(req, res) {
  var order_id = req.param('id');

  if (order_id == '*') {
    OrderState.find().exec(response);
  } else {
    OrderState.findOne({orderId: order_id}).exec(response);
  }

  function response(err, orderState) {
    if(orderState) {
      res.json(orderState);
    }
    else {
      res.status(404).end();
    }
  }
});

router.post('/:id/order_state', function(req, res) {
  var order_id = req.param('id');

  newOrderState = new OrderState({
    orderId:       order_id,
    currentState:  req.body.currentState
  });

  if (req.body.hasOwnProperty('_id')) {
      newOrderState._id = mongoose.Types.ObjectId.createFromHexString(String(req.body._id));
  }

  Order.findById(order_id).exec(create);

  function create(err, order) {
    if(order) {
      OrderState.findOne({orderId: order_id}).exec(response);

      function response(err, orderState) {
        if(orderState) {
          res.status(409).end();
        }
        else {
          newOrderState.save(function(err, newOrderState) {
            if(err) {
              res.status(400).end();
            }
            else {
              res.json(newOrderState);
            }
          });
        }
      }
    }
    else {
      res.status(404).end();
    }
  }
});

router.put('/:id/order_state', function(req, res) {
  var order_id = req.param('id');

  OrderState.findOne({orderId: order_id}).exec(response);

  function response(err, orderState) {
    if(orderState) {
      orderState.currentState = (req.body.currentState || orderState.currentState)

      orderState.save(function (err) {
        if(err) {
          res.status(500).end();
        } 
        else {
          res.status(200).end();
        }
      });
    }
    else {
      res.status(404).end();
    }
  }
});

router.delete('/:id/order_state', function(req, res) {
  var order_id = req.param('id');

  OrderState.findOne({orderId: order_id}).exec(response);

  function response(err, orderState) {
    if(orderState) {
      orderState.remove(function (err) {
        if(err) {
          res.status(500).end();
        } 
        else {
          res.status(200).end();
        }
      });
    }
    else {
      res.status(404).end();
    }
  }
});

module.exports = router;
