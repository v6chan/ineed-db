var mongoose = require('mongoose')
var express = require('express');
var router = express.Router();
var Vendor = require('../../models/vendor');

router.get('/', function(req, res) {
  var filter = req.query;

  Vendor.find(filter).exec(response);

  function response(err, vendors) {
    res.json(vendors);
  }
});

router.get('/:id', function(req, res) {
  var id = req.param('id');

  Vendor.findById(id).exec(response);

  function response(err, vendor) {
    if(vendor) {
      res.json(vendor);
    }
    else {
      res.status(404).end();
    }
  }
});

router.post('/', function(req, res) {
  vendor = new Vendor({
    name:         req.body.name,
    password:     req.body.password,
    description:  req.body.description,
    state:        req.body.state,
    city:         req.body.city,
    address:      req.body.address,
    coordinates:  req.body.coordinates,
    menu:         req.body.menu,
    email:        req.body.email,
    type:         req.body.type,
    phoneNumber:  req.body.phoneNumber,
    notiPref:     req.body.notiPref
  });

  if (req.body.hasOwnProperty('_id')) {
      vendor._id = mongoose.Types.ObjectId.createFromHexString(String(req.body._id));
  }

  vendor.save(function(err, vendor) {
    if(err) {
      res.status(400).end();
    }
    else {
      res.json(vendor);
    }
  });
});

router.put('/:id', function(req, res) {
  var id = req.param('id');

  Vendor.findById(id).exec(response);

  function response(err, vendor) {
    if(vendor) {
      vendor.name         = (req.body.name || vendor.name),
      vendor.password     = (req.body.password || vendor.password),
      vendor.description  = (req.body.description || vendor.description),
      vendor.state        = (req.body.state || vendor.state),
      vendor.city         = (req.body.city || vendor.city),
      vendor.address      = (req.body.address || vendor.address),
      vendor.coordinates  = (req.body.coordinates || vendor.coordinates),
      vendor.menu         = (req.body.menu || vendor.menu),
      vendor.email        = (req.body.email || vendor.email),
      vendor.type         = (req.body.type || vendor.type),
      vendor.phoneNumber  = (req.body.phoneNumber || vendor.phoneNumber),
      vendor.notifPref    = (req.body.notiPref || vendor.notiPref),

      vendor.save(function (err) {
        if(err) {
          res.status(500).end();
        } 
        else {
          res.status(200).end();
        }
      });
    }
    else {
      res.status(404).end();
    }
  }
});

router.delete('/:id', function(req, res) {
  var id = req.param('id');

  Vendor.findById(id).exec(response);

  function response(err, vendor) {
    if(vendor) {
      vendor.remove(function (err) {
        if(err) {
          res.status(500).end();
        } 
        else {
          res.status(200).end();
        }
      });
    }
    else {
      res.status(404).end();
    }
  }
});

module.exports = router;
